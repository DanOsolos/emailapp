import org.apache.commons.lang3.RandomStringUtils;

import java.util.Scanner;

public class Email {
    public String email;
    public String firstName;
    public String lastName;
    public String password;
    public int passwordLength;
    public String department;
    public String companySuffix = ".asdcompany.com";
    public int mailboxCapacity;
    public String alternateEmail;
    public String alternatePassword;


    public Email(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;

        System.out.println("Email created: " + this.firstName + " " + this.lastName);

        this.department = setDepartment();
        System.out.println(this.firstName + " " + this.lastName + " department is " + this.department);

        this.password = setPassword();
        System.out.println(this.firstName + " " + this.lastName + " password is " + this.password);

        System.out.print("Enter mailbox capacity in mb: ");
        this.mailboxCapacity = setMailboxCapacity();

        this.email = this.firstName.toLowerCase() + "." + this.lastName.toLowerCase()
                + "@" + this.department + this.companySuffix;

        System.out.println("The created email is: " + this.email + " and has a capacity of: " + this.mailboxCapacity + " Mb");
    }

    public String setDepartment() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Department code:\n1. Sales\n2. Development\n3. Accounting\n0.None");
        System.out.print("Enter your option: ");
        int option = scanner.nextInt();
        switch (option) {
            case 1:
                return "sales";
            case 2:
                return "development";
            case 3:
                return "accounting";
            default:
                return "";
        }
    }

    public String setPassword() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter password length: ");
        this.passwordLength = scanner.nextInt();
        System.out.println("Use letters: true/false");
        boolean useLetters = scanner.nextBoolean();
        System.out.println("Use numbers: true/false");
        boolean useNumbers = scanner.nextBoolean();

        this.password = RandomStringUtils.random(this.passwordLength, useLetters, useNumbers);

        return this.password;
    }

    public int setMailboxCapacity() {
        Scanner scanner = new Scanner(System.in);
        this.mailboxCapacity = scanner.nextInt();

        return this.mailboxCapacity;
    }

    public String getAlternateEmail() {
        return this.alternateEmail;
    }

    public String getAlternatePassword() {
        return this.alternatePassword;
    }

    public String showInformation() {
        return "Display name: " + this.firstName + " " + this.lastName +
                "\nCompany email: " + this.email +
                "\nMailbox cpacity: " + this.mailboxCapacity;
    }
}
